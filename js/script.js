var para = document.getElementsByTagName("p"); // paragrafo
var botao = document.getElementsByClassName("botao"); // botao com classe
var botaoId = document.getElementById("botao3"); // botao com ID
var body = document.getElementsByTagName("body");
var link = document.getElementsByTagName("a");
var selecao = document.getElementById("ultimaSelecao");
var n = 0;
var k = 0;
var ativado = false;
var antiga = para[0].innerHTML;
//messagem de pergunta
swal({
    title: "ATIVAR RAPTORIZE", //titulo
    showCancelButton: true,
    confirmButtonText: "sim",
    cancelButtonText: "não",
    cancelButtonColor: "#ff0000",
    confirmButtonColor: "#00dd00",
    type: "question"
}).then((result) => {
    if (result.value) {
        ativar();
    } else {
        swal("PARA ATIVAR", "CASO QUEIRA ATIVA É SÓ APERTA A TECLA \"A\"", "info");
    }
});

//switch alert
function ativar() {
    if (ativado == false) {
        swal({
            title: "Você tem certeza?",
            type: "question",
            showConfirmButton: true,
            showCloseButton: false,
            showCancelButton: true,
            cancelButtonText: "Cancelar",
            confirmButtonText: "Ativa reptorize",
            confirmButtonColor: "#ff0055"
        }).then((result) => {
            if (result.value) {
                swal({
                    title: "RAPTORIZE ATIVADO",
                    text: "AGARDE 2 SEGUNDOS, PARA ATIVALO DÊ UM CLICK NO BUTTON",
                    type: "success",
                    showConfirmButton: false,
                    showCancelButton: false,
                    timer: 2000,
                    onOpen: () => {
                        swal.showLoading();
                    }
                }).then((result => {
                    swal({
                        title: "ATENÇÃO",
                        text: "UMA VEZ ATIVA NÃO DARA MAIS PARA DESTIVÁ-LO",
                        footer: "caso queira desativá-lo reinicie a página, e click em \"não\"",
                        showConfirmButton: false,
                        time: 2000,
                        type: "info"
                    });
                }));
                alertify.set("notifier", "position", "bottom-right");
                alertify.success("RAPTORIZE ATIVADO COM SUCESSO");
                link[0].className = "myButton";
                ativado = true;
            }
        });
    } else {
        alertify.error("RAPTORIZE JÁ ESTÁ ATIVADO!!!!");
    }
}

//gira em n graus
body[0].onkeydown = function () {

    if (event.keyCode != 13) { //ENTER
        if (event.keyCode == 114 || event.keyCode == 82) {//LETRA R
            var dege = "rotate(" + n + "deg)";
            para[0].innerHTML = n + "°";
            body[0].style.transform = dege;
            n++;
        } else if (event.keyCode == 97 || event.keyCode == 65) { //ativar reptorize || LETRA A
            ativar();
        } else if (event.keyCode == 84) { // TECLA T
            swal("TECLAS", "A: ativar raptorize;</br>T: teclas;</br> R: rotação", "warning");
        } else {
            alertify.message("tecla inválida! Para saber quais teclas estão sendo usadas aperte \"T\" ")
        }
    }
};
//volta a posição inicial
body[0].onkeyup = function () {
    if (event.keyCode == 82) {
        ultima(n);
        body[0].style.transform = "rotate(0deg)";
        para[0].innerHTML = antiga;
        n = 0;
    }

};
//muda cor do botao 3 
botaoId.addEventListener("click", function () {
    botaoId.style.color = "#117c11";
});
botaoId.ondblclick = function () {
    for (var i = 0; i < para.length; i++) {
        if (para[i].style.textAlign == "left") {
            para[i].style.textAlign = "center";
        } else {
            para[i].style.textAlign = "center";
        }
    }
}
//alinha a direita
botao[0].onclick = function () {
    //alinha
    for (var i = 0; i < para.length; i++) {
        para[i].style.textAlign = "right";
    }
    //italico e cor rosa
    for (var j = 1; j < 4; j++) {
        para[j].style.fontStyle = "italic";
        para[j].style.color = "#ff5599";
    }
};
//alinha a esquerda
botao[1].onclick = function () {
    //alinha
    for (var i = 0; i < para.length; i++) {
        para[i].style.textAlign = "left";
    }
    //negrito e cor verde
    para[0].style.fontWeight = "bold";
    para[0].style.color = "green";
};
//aumenta e diminue os BOTÕES
setInterval(function () {
    if (botao[0].style.padding == "10px") {
        botao[0].style.padding = "20px";
    } else {
        botao[0].style.padding = "10px";
    }
}, 1000);
setInterval(function () {
    if (botao[0].style.padding == "10px") {
        botao[1].style.padding = "20px";
    } else {
        botao[1].style.padding = "10px";
    }
}, 1000);
setInterval(function () {
    if (botao[1].style.padding == "10px") {
        botaoId.style.padding = "20px";
    } else {
        botaoId.style.padding = "10px";
    }
}, 1000);
function ultima(n) {
    var diferente = 0;
    var opcao = document.createElement("option");
    opcao.innerHTML = n + "°";
    var verificar = document.getElementsByTagName("option");
    for (var i = 0; i < verificar.length; i++) {
        if (verificar[i].innerHTML != n + "°") {
            diferente += 1;
        }
    }
    if (diferente == verificar.length) {
        selecao.appendChild(opcao);
    }

}
function selecionado(value) {
        novo = ""
        for (var i = 0; i < value.length; i++) {
            achar = value.charAt(i);
            if (achar != "°") {
                novo += achar;
            }
        }
        var rotacao = "rotate(" + novo + "deg)";
        body[0].style.transform = rotacao;
}